package appblocker.odem.com.myapplication;

public class Constants {

    public static final String TITLE = "Title";
    public static final String LINK = "link";
    public static final String THUMB = "thumb";
    public static final String LIST_ITEMS = "ListItems";

    public static final String FINISHED_GETTING_VIDEOS = "FINISHED_GETTING_VIDEOS";

    public static final String MUSIC = "Zen Work Music";
    public static final String MOVIES = "Movie Trailers";


}
