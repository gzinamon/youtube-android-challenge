package appblocker.odem.com.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import appblocker.odem.com.myapplication.Adapter.ExpandableListAdapter;
import appblocker.odem.com.myapplication.Volley.VolleyHandler;

public class MainActivity extends AppCompatActivity {
    private final String TAG = getClass().getSimpleName();

    private VolleyHandler mVolleyHandler;
    private List<ExpandableListAdapter.Item> data;
    private JSONObject[] youTubeJsonArray;
    private LocalReceiver LocalReceiver;
    private RecyclerView mRecyclerView;
    private ExpandableListAdapter mExpandableListAdapter;


    @Override
    protected void onResume() {
        super.onResume();
        LocalReceiver = new LocalReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.FINISHED_GETTING_VIDEOS);
        registerReceiver(LocalReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (LocalReceiver != null)
            unregisterReceiver(LocalReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mVolleyHandler = new VolleyHandler(this);
        mVolleyHandler.getVideosFromYouTube();
        data = new ArrayList<>();
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);


    }

    private List fillListForAdapter(JSONObject[] youTubeJsonObject) {
        data.clear();

        if (mRecyclerView != null) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this
                    , LinearLayoutManager.VERTICAL, false));
        }

        try {
            for (int j = 0; j < youTubeJsonObject.length; j++) {
                JSONArray jsonArray = youTubeJsonObject[j].getJSONArray(Constants.LIST_ITEMS);
                String title = youTubeJsonObject[j].getString("ListTitle");
                boolean playable = false;
                data.add(new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER
                        , title));
                //fill list with items
                for (int i = 0; i < jsonArray.length(); i++) {
                    if (title.equalsIgnoreCase(Constants.MOVIES))
                        playable = true;

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    data.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD
                            , jsonObject.getString(Constants.TITLE)
                            , jsonObject.getString(Constants.LINK)
                            , jsonObject.getString(Constants.THUMB)
                            ,playable
                    ));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mExpandableListAdapter = new ExpandableListAdapter(this, data);
        mRecyclerView.setAdapter(mExpandableListAdapter);
        findViewById(R.id.progress_bar).setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        return data;
    }


    public class LocalReceiver extends BroadcastReceiver {
        String TAG = "LocalReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action) {
                case Constants.FINISHED_GETTING_VIDEOS:

                    youTubeJsonArray = mVolleyHandler.youTubeJsonArray;
                    if (youTubeJsonArray != null) {
                        Log.e(TAG, "got playlist : " + youTubeJsonArray);
                        fillListForAdapter(youTubeJsonArray);

                    } else {
                        Log.e("youtube playlist null", "null");
                    }


                    break;
            }
        }

    }


}
