package appblocker.odem.com.myapplication.Volley;

import android.content.Context;
import android.content.Intent;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import appblocker.odem.com.myapplication.Constants;

public class VolleyHandler {

    private final String TAG = getClass().getSimpleName();
    private final String url = "http://www.razor-tech.co.il/hiring/youtube-api.json";
    private Context mContext;
    public JSONObject[] youTubeJsonArray = null;

    public VolleyHandler(Context context) {
        mContext = context;

    }


    public void getVideosFromYouTube() {

        RequestQueue mRequestQueue = Volley.newRequestQueue(mContext);


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //success
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    youTubeJsonArray = createArrayFromJson(jsonObject);
                    mContext.sendBroadcast(new Intent(Constants.FINISHED_GETTING_VIDEOS));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        mRequestQueue.add(stringRequest);


    }

    public static void getImageFromServer(Context mContext, String url, NetworkImageView imageView) {
//        RequestQueue mRequestQueue = Volley.newRequestQueue(mContext);
        ImageLoader.ImageCache imageCache = new BitmapLruCache();
        ImageLoader imageLoader = new ImageLoader(Volley.newRequestQueue(mContext), imageCache);
        imageView.setImageUrl(url, imageLoader);


    }


    public JSONObject[] createArrayFromJson(JSONObject razorJson) {
        JSONObject[] jsonObjects = new JSONObject[2];
        try {
            JSONArray jsonPlaylists = razorJson.getJSONArray("Playlists");
            JSONObject zenWorkMusicJson = jsonPlaylists.getJSONObject(0);
            JSONObject movieTrailersJson = jsonPlaylists.getJSONObject(1);

            jsonObjects[0] = zenWorkMusicJson;
            jsonObjects[1] = movieTrailersJson;

//            String musicListTitle = zenWorkMusicJson.getString("ListTitle");
//            String movieListTitle = movieTrailersJson.getString("ListTitle");
//
//            JSONArray zenWorkMusicListItems = zenWorkMusicJson.getJSONArray(Constants.LIST_ITEMS);
//            JSONArray movieTrailersListItems = movieTrailersJson.getJSONArray(Constants.LIST_ITEMS);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObjects;
    }

}
