package appblocker.odem.com.myapplication.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.google.android.youtube.player.YouTubePlayer;
import com.thefinestartist.ytpa.YouTubePlayerActivity;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.utils.YouTubeUrlParser;

import java.util.ArrayList;
import java.util.List;

import appblocker.odem.com.myapplication.R;
import appblocker.odem.com.myapplication.Volley.VolleyHandler;

public class ExpandableListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final String TAG = getClass().getSimpleName();

    public static final int HEADER = 0;
    public static final int CHILD = 1;
    public Context mContext;

    private List<Item> data;

    public ExpandableListAdapter(Context context, List<Item> mdata) {
        mContext = context;
        data = mdata;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = null;
        LayoutInflater inflater = (LayoutInflater) parent.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        switch (type) {
            case HEADER:
                view = inflater.inflate(R.layout.list_header, parent, false);
                ListItemsViewHolder header = new ListItemsViewHolder(view, HEADER);

                return header;
            case CHILD:
                view = inflater.inflate(R.layout.list_item, parent, false);
                ListItemsViewHolder child = new ListItemsViewHolder(view, CHILD);

                return child;
        }
        return null;
    }

    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final Item item = data.get(position);

        switch (item.type) {
            case HEADER:
                final ListItemsViewHolder itemController = (ListItemsViewHolder) holder;
                itemController.refferalItem = item;
                itemController.header_title.setText(item.title);
                if (item.invisibleChildren == null) {
                    itemController.btn_expand_toggle.setImageResource(R.drawable.circle_minus);
                } else {
                    itemController.btn_expand_toggle.setImageResource(R.drawable.circle_plus);
                }
                itemController.btn_expand_toggle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (item.invisibleChildren == null) {
                            item.invisibleChildren = new ArrayList<Item>();
                            int count = 0;
                            int pos = data.indexOf(itemController.refferalItem);
                            while (data.size() > pos + 1 && data.get(pos + 1).type == CHILD) {
                                item.invisibleChildren.add(data.remove(pos + 1));
                                count++;
                            }
                            notifyItemRangeRemoved(pos + 1, count);
                            itemController.btn_expand_toggle.setImageResource(R.drawable.circle_plus);
                        } else {
                            int pos = data.indexOf(itemController.refferalItem);
                            int index = pos + 1;
                            for (Item i : item.invisibleChildren) {
                                data.add(index, i);
                                index++;
                            }
                            notifyItemRangeInserted(pos + 1, index - pos - 1);
                            itemController.btn_expand_toggle.setImageResource(R.drawable.circle_minus);
                            item.invisibleChildren = null;
                        }
                    }
                });
                break;
            case CHILD:

                ListItemsViewHolder itemsViewHolder = (ListItemsViewHolder) holder;
                itemsViewHolder.tv_title.setText(item.title);
                itemsViewHolder.tv_link.setText(item.link);

                VolleyHandler.getImageFromServer(mContext, item.thumb, itemsViewHolder.niv_thumb);
                if (item.isPlayable)
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startYouTubePlayer(item.link);
                        }
                    });
                break;
        }
    }

    private void startYouTubePlayer(String link) {
        Intent intent = new Intent(mContext, YouTubePlayerActivity.class);

        // Youtube video ID (Required, You can use YouTubeUrlParser to parse Video Id from url)
        intent.putExtra(YouTubePlayerActivity.EXTRA_VIDEO_ID, YouTubeUrlParser.getVideoId(link));

        // Youtube player style (DEFAULT as default)
        intent.putExtra(YouTubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT);

        // Screen Orientation Setting (AUTO for default)
        // AUTO, AUTO_START_WITH_LANDSCAPE, ONLY_LANDSCAPE, ONLY_PORTRAIT
        intent.putExtra(YouTubePlayerActivity.EXTRA_ORIENTATION, Orientation.AUTO);

        // Show audio interface when user adjust volume (true for default)
        intent.putExtra(YouTubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true);

        // If the video is not playable, use Youtube app or Internet Browser to play it
        // (true for default)
        intent.putExtra(YouTubePlayerActivity.EXTRA_HANDLE_ERROR, true);

        // Animation when closing youtubeplayeractivity (none for default)
        intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_ENTER, android.R.anim.fade_in);
        intent.putExtra(YouTubePlayerActivity.EXTRA_ANIM_EXIT, android.R.anim.fade_out);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }


    @Override
    public int getItemViewType(int position) {
        return data.get(position).type;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    private static class ListItemsViewHolder extends RecyclerView.ViewHolder {
        public TextView header_title;
        public ImageView btn_expand_toggle;
        public Item refferalItem;

        public TextView tv_title;
        public TextView tv_link;
        public NetworkImageView niv_thumb;


        public ListItemsViewHolder(View itemView, int type) {

            super(itemView);
            switch (type) {
                case HEADER:
                    header_title = (TextView) itemView.findViewById(R.id.header_title);
                    btn_expand_toggle = (ImageView) itemView.findViewById(R.id.btn_expand_toggle);
                    break;

                case CHILD:
                    tv_title = (TextView) itemView.findViewById(R.id.tv_title);
                    tv_link = (TextView) itemView.findViewById(R.id.tv_link);
                    niv_thumb = (NetworkImageView) itemView.findViewById(R.id.niv_thumb);
                    break;
            }
        }
    }

    public static class Item {
        public int type;
        public String title;
        public String link;
        public String thumb;
        public boolean isPlayable;

        public List<Item> invisibleChildren;

        public Item() {
        }

        public Item(int type, String title, String link, String thumb, boolean playable) {
            this.type = type;
            this.title = title;
            this.link = link;
            this.thumb = thumb;
            this.isPlayable = playable;
        }

        public Item(int type, String text) {
            this.type = type;
            this.title = text;
        }
    }
}
